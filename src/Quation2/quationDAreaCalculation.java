package Quation2;

import java.util.Scanner;

public class quationDAreaCalculation {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter the shape (rectangle, square, or circle): ");
        String shape = sc.nextLine();
        System.out.println();

        if (shape.equals("rectangle")) {
            System.out.print("Enter the length: ");
            double length = sc.nextDouble();
            System.out.print("Enter the breadth: ");
            double breadth = sc.nextDouble();
            double area = length * breadth;
            System.out.println("Area of the rectangle is: " + area);
        } else if (shape.equals("square")) {
            System.out.print("Enter the side: ");
            double side = sc.nextDouble();
            double area = side * side;
            System.out.println("Area of the square is: " + area);
        } else if (shape.equals("circle")) {
            System.out.print("Enter the radius: ");
            double radius = sc.nextDouble();
            double area = Math.PI * radius * radius;
            System.out.println("Area of the circle is: " + area);
        } else {
            System.out.println("Invalid shape entered.");
        }
    }
}
